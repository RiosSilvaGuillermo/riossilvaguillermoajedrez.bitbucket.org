var Ajedrez = (function(){
	var _mostrar_tablero = function(){
		var xhr = new XMLHttpRequest();
		var divMen=document.getElementById("mensaje");
		xhr.onreadystatechange=function(){
			if(this.readyState===4){
				if(this.status===200){
					_procesa_tablero(this.responseText);
				}else if(this.status!=200){
					var mensa=document.createElement("p");
					mensa.textContent="ERROR CON EL PROCESADO DEL TABLERO "+this.responseURL;
					divMen.appendChild(mensa);
				}
			}
		};
		xhr.open('GET',"http://riossilvaguillermoajedrez.bitbucket.org/csv/tablero.csv",true);
		xhr.send();
	};

	var _procesa_tablero =function(tablero){
		var letras = ["a","b","c","d","e","f","g","h"];
		var numeros = [9,8,7,6,5,4,3,2,1];
		var div=document.getElementById("tablero");
		if(div.firstChild){
			div.removeChild(div.firstChild);
		}
		var table=document.createElement("table");
		div.appendChild(table);
		var table_tbody=document.createElement("tbody");
		table.appendChild(table_tbody);
		var rows = tablero.split("\n");
		for(var i=1;i<rows.length;i++){
			var table_tr=document.createElement("tr");
			var cells = rows[i].split(/,/);
			for(var j=0;j<cells.length;j++){
				if(cells[j]==="\\u2656"){
					cells[j]="\u2656";
				}else if(cells[j]==="\\u2659"){
					cells[j]="\u2659"
				}else if(cells[j]==="\\u265F"){
					cells[j]="\u265F"
				}else if(cells[j]==="\\u2658"){
					cells[j]="\u2658"
				}else if(cells[j]==="\\u2657"){
					cells[j]="\u2657"
				}else if(cells[j]==="\\u2656"){
					cells[j]="\u2656"
				}else if(cells[j]==="\\u2655"){
					cells[j]="\u2655"
				}else if(cells[j]==="\\u2654"){
					cells[j]="\u2654"
				}else if(cells[j]==="\\u265A"){
					cells[j]="\u265A"
				}else if(cells[j]==="\\u265B"){
					cells[j]="\u265B"
				}else if(cells[j]==="\\u265C"){
					cells[j]="\u265C"
				}else if(cells[j]==="\\u265D"){
					cells[j]="\u265D"
				}else if(cells[j]==="\\u265E"){
					cells[j]="\u265E"
				}
				var table_td = document.createElement("td");
				table_td.textContent=cells[j];
				table_td.id=letras[j]+numeros[i];
				table_tr.appendChild(table_td);
			}
			table_tbody.appendChild(table_tr);
		}
	};

	var _actualizar_tablero= function(){
		var divMensa = document.getElementById("mensaje");
		if(divMensa.firstChild){
			divMensa.removeChild(divMensa.firstChild);
		}
		var divTablero=document.getElementById("tablero");
		if(divTablero.firstChild){
			divTablero.removeChild(divTablero.firstChild);
		}
		_mostrar_tablero();
		console.log("Tablero Descargado");

	};



	var _mover_pieza=function(movimiento){
		var divmen=document.getElementById("mensaje");
		var mensaje = document.createElement("p");
		var movDe=document.getElementById(movimiento["de"]);
		var movA=document.getElementById(movimiento["a"]);
		divmen.appendChild(mensaje);
		if(movDe.textContent==="X"){
			mensaje.textContent="No hay pieza seleccionada.";
		}else{
			if(movA.textContent!=="X"){
				mensaje.textContent="El lugar esta ocupado por pieza.";
			}else{
				movA.textContent=movDe.textContent;
				movDe.textContent="X";
				mensaje.textContent="Pieza "+movA.textContent+" movida.";
			}
		}
		
	};

	return{
		"mostrarTablero":_mostrar_tablero,
		"actualizarTablero":_actualizar_tablero,
		"moverPieza":_mover_pieza,
	}
})();
